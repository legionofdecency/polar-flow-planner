// Package api handles making requests to Polar Flow's API.
package api

import "bitbucket.org/legionofdecency/polar-flow-planner/schedule"

type FlowClient interface {
	ScheduleActivity(schedule.ScheduledActivity) error
}

// Client returns a client implementation.
func Client(AuthCookie string) (FlowClient, error) {
	return &client{AuthCookie}, nil
}

// client represents an API client that implements the FlowClient interface.
type client struct {
	// AuthCookie is the value of the authentication cookie for accessing the API.
	AuthCookie string
}

// ScheduleActivity schedules an activity at the specified time.
func (c *client) ScheduleActivity(schedule.ScheduledActivity) error {

	return nil
}
