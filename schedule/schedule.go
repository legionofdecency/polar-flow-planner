package schedule

import "time"

const OneDay = time.Hour * 24

// Activity represents a
type Activity struct {
	// Name is the human-friendly name of the activity.
	Name string
	// Identifier is the numeric identifier within Polar Flow of the activity.
	Identifier int64
}

// Cadence defines weekdays and times of day an activity is to be performed.
type Cadence struct {
	// Days defines the weekdays on which an activity is to be performed.
	Days []time.Weekday
	// TimesOfDay are the times of day an activity is to be performed.
	TimesOfDay []time.Duration
	// Start is the start time of the schedule.
	Start time.Time
	// End is the end time of the schedule.
	End time.Time
}

// ScheduledActvity is an activity plus a specific time when it is to be performed.
type ScheduledActivity struct {
	// Activity is the activity to be performed.
	Activity
	// Time is the time the activity is to be performed.
	time.Time
}

// Generate creates ScheduledActivities from an Activity and a Cadence
func Generate(a Activity, c Cadence) []ScheduledActivity {
	var ret []ScheduledActivity
	i := c.Start
	for c.End.After(i) {
		for _, tod := range c.TimesOfDay {
			day := i.Truncate(OneDay)
			t := day.Add(tod)
			if t.After(i) {
				ret = append(ret, ScheduledActivity{Activity: a, Time: t})
			}
		}
		i = i.AddDate(0, 0, 1)
	}
	return ret
}
